package utility;

import utility.Constants.Agents;

// =========================================================================================================
// Aggro.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// This utility class keeps informaton about the enum type, Aggros, and helps transition from aggro states
// depending on the action taken and the identity of the agent.
// =========================================================================================================

public final class Aggro 
{
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public enum Aggros
	{
		LOW_AGGRO,
		MEDIUM_AGGRO,
		HIGH_AGGRO,
		DEAD_AGGRO
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	private Aggro() {}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETAGGRO()
	// --------------------------------------------------------------------------------------------------
	// Depending on what kind of agent and action it is, an aggro increase/decrease will vary. This method
	// will return what that increase/decrease would be.
	// @params Action action, int aggroAgent
	// @returns Aggros aggro
	// ===================================================================================================
	public static Aggros getAggro(Action action, int agentID)
	{
		Actions actionType = action.getActionType();
		Aggros aggro = Aggros.LOW_AGGRO;
			
		// ENEMY
		if(agentID == Agents.ENEMY.ordinal())
		{
			switch(actionType)
			{
				case ATTACK_ACTION:
				{
					aggro = Aggros.HIGH_AGGRO;
					break;
				}
				case DEFEND_ACTION:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
				case SPECIAL_HEAL_ACTION:
				{
					aggro = Aggros.MEDIUM_AGGRO;
					break;
				}
				default:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
			}	
		}
		// HEALER
		else if(agentID == Agents.HEALER.ordinal())
		{
			switch(actionType)
			{
				case ATTACK_ACTION:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
				case DEFEND_ACTION:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
				case SPECIAL_HEAL_ACTION:
				{
					aggro = Aggros.HIGH_AGGRO;
					break;
				}
				default:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
			}
		}
		// ROGUE
		else if(agentID == Agents.ROGUE.ordinal())
		{
			switch(actionType)
			{
				case ATTACK_ACTION:
				{
					aggro = Aggros.MEDIUM_AGGRO;
					break;
				}
				case DEFEND_ACTION:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
				case SPECIAL_TRICK_ACTION:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
				default:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
			}
		}
		// WARRIOR
		else if(agentID == Agents.WARRIOR.ordinal())
		{
			switch(actionType)
			{
				case ATTACK_ACTION:
				{
					aggro = Aggros.HIGH_AGGRO;
					break;
				}
				case DEFEND_ACTION:
				{
					aggro = Aggros.MEDIUM_AGGRO;
					break;
				}
				case SPECIAL_TAUNT_ACTION:
				{
					aggro = Aggros.HIGH_AGGRO;
					break;
				}
				default:
				{
					aggro = Aggros.LOW_AGGRO;
					break;
				}
			}
		}
		
		return aggro;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
}
