package utility;

// =========================================================================================================
// Constants.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// Keeps track of constants needed for the game.
// =========================================================================================================

public final class Constants 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// BASIC GAME INFO
	// ===================================================================================================
	
	// Agent types
	public enum Agents { ENEMY, HEALER, ROGUE, WARRIOR }
	
	public static final int ENEMY_ID = Agents.ENEMY.ordinal();
	public static final int HEALER_ID = Agents.HEALER.ordinal();
	public static final int WARRIOR_ID = Agents.WARRIOR.ordinal();
	public static final int ROGUE_ID = Agents.ROGUE.ordinal();
	
	public static final int NOT_DEFENDING = 0;
	public static final int IS_DEFENDING = 1;
	
	public static final int NUM_AGENTS 			= 4;
	public static final int NUM_PARTY_AGENTS 	= NUM_AGENTS - 1;
	
	// ===================================================================================================
	// AGENT SPECIAL COUNT, DAMAGE, AND HEALTH
	// ===================================================================================================
	
	// Base agent info
	public static final int BASE_HEALTH 	= 10;
	public static final int BASE_DAMAGE 	= 5;
	public static final int SPECIAL_COUNT 	= 1;
	
	// Special numbers info
	public static final int HEAL_AMOUNT 	= BASE_DAMAGE * 2;
	public static final int DEFEND_AMOUNT 	= BASE_DAMAGE;
	
	// Damage info
	public static final int HEALER_DAMAGE	= BASE_DAMAGE * 1;
	public static final int ROGUE_DAMAGE	= BASE_DAMAGE * 3;
	public static final int WARRIOR_DAMAGE 	= BASE_DAMAGE * 2;
	public static final int ENEMY_DAMAGE	= BASE_DAMAGE * 5;
	
	// Health info
	public static final int HEALER_HEALTH	= BASE_HEALTH * 2;
	public static final int ROGUE_HEALTH	= BASE_HEALTH * 3;
	public static final int WARRIOR_HEALTH	= BASE_HEALTH * 4;
	public static final int ENEMY_HEALTH	= BASE_HEALTH * 12;
	
	// Special info
	public static final int HEALER_SPECIAL_COUNT 	= SPECIAL_COUNT + 2;
	public static final int ROGUE_SPECIAL_COUNT 	= SPECIAL_COUNT;
	public static final int WARRIOR_SPECIAL_COUNT 	= SPECIAL_COUNT;
	public static final int ENEMY_SPECIAL_COUNT 	= SPECIAL_COUNT;
	
	// ===================================================================================================
	// CHANCES TO HIT (SUCCEED)
	// ===================================================================================================
	public static final double HEALER_CHANCE_TO_HIT 	= 0.5;
	public static final double ROGUE_CHANCE_TO_HIT 		= 0.5;
	public static final double WARRIOR_CHANCE_TO_HIT 	= 0.5;
	public static final double ENEMY_CHANCE_TO_HIT 		= 0.8;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	private Constants() {}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////

}
