package utility;

// =========================================================================================================
// Action.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// This contains information about the action to be performed by an agent which includes the source agent,
// the source agent's chance to hit, the target agent, and the action type that will be performed.
// =========================================================================================================

public class Action 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ===================================================================================================
	// ATTRIBUTES
	// ===================================================================================================
	
	private int sourceAgent;
	private double sourceChanceToHit;
	private int targetAgent;
	private Actions actionType;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	public Action(int sourceAgent, double sourceChanceToHit, int targetAgent, Actions actionType)
	{
		this.sourceAgent = sourceAgent;
		this.sourceChanceToHit = sourceChanceToHit;
		this.targetAgent = targetAgent;
		this.actionType = actionType;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETTERS
	// ===================================================================================================
	
	public int getSourceAgent() { return sourceAgent; }
	public int getTargetAgent() { return targetAgent; }
	public double getSourceChanceToHit() { return sourceChanceToHit; }
	public Actions getActionType() { return actionType; }
	public boolean isSpecial()
	{
		return (actionType == Actions.SPECIAL_HEAL_ACTION || 
				actionType == Actions.SPECIAL_TAUNT_ACTION || 
				actionType == Actions.SPECIAL_TRICK_ACTION);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
}
