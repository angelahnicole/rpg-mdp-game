package agents;

import java.util.HashMap;

import mdp.MDP;
import mdp.ValueIteration;
import driver.Game;
import utility.*;
import utility.Aggro.Aggros;

// =========================================================================================================
// Agent.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// This class defines what all agents have in common and helps define some of the game mechanics (i.e. 
// updating aggro, healing, attacking, etc.) Also helps create a policy for the agent.
// =========================================================================================================

public abstract class Agent 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ===================================================================================================
	// ATTRIBUTES
	// ===================================================================================================	
	
	// The rest of these are set by the children classes
	protected int maxHealth;
	protected int ID;
	protected int health;
	protected int damage;
	protected Aggros aggro;
	protected double chanceToHit;
	protected int specialCount;
	protected boolean isDefending;
	private ValueIteration myValueIteration;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	public Agent()
	{
		this.aggro = Aggros.LOW_AGGRO;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CHOOSEMOVE()
	// --------------------------------------------------------------------------------------------------
	// Uses the policy developed in ValueIteration to choose an action depending on the state.
	// @returns Action 
	// ===================================================================================================	
	public Action chooseMove()
	{
		State currentState = Game.getInstance().getMDP().getStates()[ID][specialCount][aggro.ordinal()];
		Action chosenAction = myValueIteration.getPolicy(currentState);
		
		return chosenAction;
	}
	
	// ===================================================================================================
	// CREATEPOLICY()
	// --------------------------------------------------------------------------------------------------
	// Using an MDP framework and Value Iteration, create a policy for the agent to follow.
	// ===================================================================================================	
	public void createPolicy()
	{
		MDP myMDP = Game.getInstance().getMDP();
		myValueIteration = new ValueIteration(myMDP, 500, 0.5, ID);
		myValueIteration.iterateValues();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// SETUPTURN()
	// --------------------------------------------------------------------------------------------------
	// Sets isDefending to false (so they will only defend for one turn)
	// ===================================================================================================	
	public void setupTurn()
	{
		isDefending = false;
	}
	
	// ===================================================================================================
	// UPDATEAGGRO()
	// --------------------------------------------------------------------------------------------------
	// Transitions to different aggros depending on action taken and type of agent making the action.
	// @params Action action
	// ===================================================================================================	
	public void updateAggro(Action action)
	{
		aggro = Aggro.getAggro(action, ID);
	}
	
	// ===================================================================================================
	// SUBTRACTHEALTH()
	// --------------------------------------------------------------------------------------------------
	// Subtracts health from the agent. If they are defending, then we'll subtract the amount of damage
	// taken to the amount the agent is defending. If the health is lowered to 0, then the agent is
	// dead.
	// @params int damage
	// ===================================================================================================	
	public void subtractHealth(int damage) 
	{ 
		int netDamage = damage;
		
		if(isDefending)
		{		
			if(Constants.DEFEND_AMOUNT <= damage)
				netDamage = damage - Constants.DEFEND_AMOUNT;
			else
				netDamage = 0;				
		}

		if(netDamage > health)
		{
			health = 0;
		}
		else
			health = health - netDamage;	
	}
	
	// ===================================================================================================
	// ADDHEALTH()
	// --------------------------------------------------------------------------------------------------
	// Adds health to the agent. If the amount of healing exceeds their max health, then it sets the 
	// agent's health to their maximum health (so the agent can't heal themselves to increase their
	// health)
	// @params int heal
	// ===================================================================================================
	public void addHealth(int heal)
	{
		if(heal + health > maxHealth)
			health = maxHealth;
		else
			health = health + heal;
	}
	
	// ===================================================================================================
	// MAKEDEFENDINGTRUE()
	// --------------------------------------------------------------------------------------------------
	// Set the agent's isDefending attribute to true.
	// ===================================================================================================
	public void makeDefendingTrue()
	{
		isDefending = true;
	}
	
	// ===================================================================================================
	// DECREMENTSPECIALCOUNT()
	// --------------------------------------------------------------------------------------------------
	// Subtract one from the agent's special count.
	// ===================================================================================================
	public void decrementSpecialCount()
	{
		specialCount--;
	}
	
	// ===================================================================================================
	// TOSTRING()
	// ===================================================================================================
	public String toString()
	{
		return 	"Agent";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETTERS
	// ===================================================================================================
	
	public int getID() { return ID; }
	public int getHealth() { return health; }
	public double getChanceToHit() { return chanceToHit; }
	public int getDamage() { return damage; }
	public boolean isDead() { return health == 0; }
	public Aggros getAggro() { return aggro; }
	public HashMap<State, Action> getPolicies() { return myValueIteration.getPolicies(); }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////

}
