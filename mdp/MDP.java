package mdp;

import java.util.ArrayList;
import java.util.HashMap;
import utility.Action;
import utility.Actions;
import utility.Aggro.Aggros;
import utility.Constants;
import utility.Aggro;
import utility.GenMath;
import utility.State;
import utility.Tuple;

// =========================================================================================================
// MDP.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// This class helps set up MDP for Value Iteration by creating states and returning rewards and state
// transitions and probabilities.
// =========================================================================================================

public class MDP 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ===================================================================================================
	// ATTRIBUTES
	// ===================================================================================================
	
	private State[][][] states;
	private int[][] stateBounds;
	private int SPECIAL_MAX_BOUND;
	private int AGGRO_MAX_BOUND;
	public final int SPECIAL_BOUND = 0;
	public final int AGGRO_BOUND = 1;
		
	private HashMap<Integer, Action[]> AgentActions;
	private HashMap<Integer, Action[]> AgentNoSpecialActions;
	private HashMap<Integer, Integer[]> AgentAttributes;
	private double[][] AgentDeathRewards;
	private double[] AgentAggroRewards;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	public MDP()
	{
		makeStates();	
		makeActions();
		makeNoSpecialActions();	
		makeAttributes();	
		makeRewards();
		makeAggroRewards();
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETPOSSIBLEACTIONS()
	// --------------------------------------------------------------------------------------------------
	// Returns the possible actions in the state depending on what agent is given.
	// @params State state, int ID
	// @returns Action[] actions
	// ===================================================================================================
	public Action[] getPossibleActions(State state, int ID)
	{
		if(state.getSpecial() > 0)
			return AgentActions.get(ID);
		else
			return AgentNoSpecialActions.get(ID);
	}
	
	// ===================================================================================================
	// GETTRANSITIONANDPROBS()
	// --------------------------------------------------------------------------------------------------
	// Retrieves the possible states it could transition into from noise and also returns the probability
	// of it happening.
	// @params State state, Action action
	// @returns ArrayList<Tuple<State, Double>> stateProbs
	// ===================================================================================================
	public ArrayList<Tuple<State, Double>> getTransitionAndProbs(State state, Action action)
	{
		int agent = state.getAgent();
		int special = state.getSpecial();
		int oldAggro = state.getAggro();
		ArrayList<Tuple<State, Double>> stateProbs;
		
		if(action.isSpecial())
			special--;
		
		if(!state.isDead())
		{
			int failAggro = Aggro.Aggros.LOW_AGGRO.ordinal();
			int successAggro = Aggro.getAggro(action, agent).ordinal();
			double success = action.getSourceChanceToHit();
			double fail = (1 - success);
			
			State successState = new State(agent, special, successAggro);
			State failState = new State(agent, special, failAggro);
			
			stateProbs = new ArrayList<>(2);
			stateProbs.add(new Tuple<State, Double>(successState, success));
			stateProbs.add(new Tuple<State, Double>(failState, fail));
		}
		else
		{
			State deadState = new State(agent, special, oldAggro);
			double deadProb = 1.0;
			
			stateProbs = new ArrayList<>(1);
			stateProbs.add(new Tuple<State, Double>(deadState, deadProb));	
		}
			
		return stateProbs;	
	}
	
	// ===================================================================================================
	// GETREWARD()
	// --------------------------------------------------------------------------------------------------
	// Rewards the agent for their current state of aggro or, if they are dead, reward them for that as 
	// well.
	// ===================================================================================================
	public double getReward(State state, Action action, State nextState, int agentID)
	{
		double reward = 0.0;
		int sourceAgent = nextState.getAgent();
		int sourceAggro = nextState.getAggro();
		
		// Return a non-zero reward if an agent is dead
		if(nextState.isDead())
		{
			reward += AgentDeathRewards[agentID][sourceAgent];
		}
		
		reward += AgentAggroRewards[sourceAggro];
		
		return reward;	
	}
	
	// ===================================================================================================
	// GETSTATES()
	// --------------------------------------------------------------------------------------------------
	// Returns the state space.
	// ===================================================================================================
	public State[][][] getStates()
	{
		return states;
	}
	
	// ===================================================================================================
	// GETSTATEBOUNDS()
	// --------------------------------------------------------------------------------------------------
	// Returns the bounds of the state attributes for each agent.
	// ===================================================================================================
	public int[][] getStateBounds()
	{
		return stateBounds;
	}
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// MAKESTATEBOUNDS()
	// --------------------------------------------------------------------------------------------------
	// Creates the bounds of state attributes for each agent.
	// ===================================================================================================
	private void makeStateBounds()
	{
		stateBounds = new int[4][4];
	
		// SPECIAL COUNT
		stateBounds[SPECIAL_BOUND][Constants.ENEMY_ID] = Constants.ENEMY_SPECIAL_COUNT + 1;
		stateBounds[SPECIAL_BOUND][Constants.HEALER_ID] = Constants.HEALER_SPECIAL_COUNT + 1;
		stateBounds[SPECIAL_BOUND][Constants.WARRIOR_ID] = Constants.WARRIOR_SPECIAL_COUNT + 1;
		stateBounds[SPECIAL_BOUND][Constants.ROGUE_ID] = Constants.ROGUE_SPECIAL_COUNT + 1;
		SPECIAL_MAX_BOUND = GenMath.getMax(stateBounds[SPECIAL_BOUND]);
		//AGGRO
		stateBounds[AGGRO_BOUND][Constants.ENEMY_ID] = Aggro.Aggros.DEAD_AGGRO.ordinal() + 1;
		stateBounds[AGGRO_BOUND][Constants.HEALER_ID] = Aggro.Aggros.DEAD_AGGRO.ordinal() + 1;
		stateBounds[AGGRO_BOUND][Constants.WARRIOR_ID] = Aggro.Aggros.DEAD_AGGRO.ordinal() + 1;
		stateBounds[AGGRO_BOUND][Constants.ROGUE_ID] = Aggro.Aggros.DEAD_AGGRO.ordinal() + 1;
		AGGRO_MAX_BOUND = Aggro.Aggros.DEAD_AGGRO.ordinal() + 1;
	}
	
	// ===================================================================================================
	// MAKESTATES()
	// --------------------------------------------------------------------------------------------------
	// Creates the state space based on agents, their special counts, and their aggros.
	// ===================================================================================================
	public void makeStates()
	{
		// Make bounds for each agent (every agent has different upper bounds)
		makeStateBounds();
		
		states = new State[Constants.NUM_AGENTS][SPECIAL_MAX_BOUND][AGGRO_MAX_BOUND];
		
		// Agents		
		for(int a = 0; a < Constants.NUM_AGENTS; a++)
		{
			// Special Count
			for(int s = 0; s < stateBounds[SPECIAL_BOUND][a]; s++)
			{
				// Aggro
				for(int r = 0; r < stateBounds[AGGRO_BOUND][a]; r++)
				{
					states[a][s][r] = new State(a, s, r);
				}
			} // special
		} // agents
	}
	
	// ===================================================================================================
	// MAKEACTIONS()
	// --------------------------------------------------------------------------------------------------
	// Creates possible actions for each agent.
	// ===================================================================================================
	private void makeActions()
	{
		AgentActions = new HashMap<>();
		
		// Agent actions
		Action[] enemyActions = 
		{
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.HEALER_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.DEFEND_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.SPECIAL_HEAL_ACTION) 
		};
		Action[] healerActions = 
		{
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.HEALER_ID, Actions.DEFEND_ACTION), 
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.HEALER_ID, Actions.SPECIAL_HEAL_ACTION),
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.SPECIAL_HEAL_ACTION), 
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.SPECIAL_HEAL_ACTION)
		};
		Action[] warriorActions = 
		{
				new Action(Constants.WARRIOR_ID, Constants.WARRIOR_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.WARRIOR_ID, Constants.WARRIOR_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.DEFEND_ACTION), 
				new Action(Constants.WARRIOR_ID, Constants.WARRIOR_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.SPECIAL_TAUNT_ACTION) 
		};
		Action[] rogueActions = 
		{
				new Action(Constants.ROGUE_ID, Constants.ROGUE_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ROGUE_ID, Constants.ROGUE_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.DEFEND_ACTION), 
				new Action(Constants.ROGUE_ID, Constants.ROGUE_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.SPECIAL_TRICK_ACTION) 
		};
		
		// Add to hash map of actions
		AgentActions.put(Constants.ENEMY_ID, enemyActions);
		AgentActions.put(Constants.HEALER_ID, healerActions);
		AgentActions.put(Constants.WARRIOR_ID, warriorActions);
		AgentActions.put(Constants.ROGUE_ID, rogueActions);	
	}
	
	// ===================================================================================================
	// MAKENOSPECIALACTIONS()
	// --------------------------------------------------------------------------------------------------
	// Creates possible actions for each agent that does not include their specials.
	// ===================================================================================================
	private void makeNoSpecialActions()
	{
		AgentNoSpecialActions = new HashMap<Integer, Action[]>();
		
		// Agent actions
		Action[] enemyActions = 
		{
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.HEALER_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ENEMY_ID, Constants.ENEMY_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.DEFEND_ACTION)
		};
		Action[] healerActions = 
		{
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.HEALER_ID, Constants.HEALER_CHANCE_TO_HIT, Constants.HEALER_ID, Actions.DEFEND_ACTION)
		};
		Action[] warriorActions = 
		{
				new Action(Constants.WARRIOR_ID, Constants.WARRIOR_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.WARRIOR_ID, Constants.WARRIOR_CHANCE_TO_HIT, Constants.WARRIOR_ID, Actions.DEFEND_ACTION)
		};
		Action[] rogueActions = 
		{
				new Action(Constants.ROGUE_ID, Constants.ROGUE_CHANCE_TO_HIT, Constants.ENEMY_ID, Actions.ATTACK_ACTION), 
				new Action(Constants.ROGUE_ID, Constants.ROGUE_CHANCE_TO_HIT, Constants.ROGUE_ID, Actions.DEFEND_ACTION)
		};
		
		// Add to hash map of actions
		AgentNoSpecialActions.put(Constants.ENEMY_ID, enemyActions);
		AgentNoSpecialActions.put(Constants.HEALER_ID, healerActions);
		AgentNoSpecialActions.put(Constants.WARRIOR_ID, warriorActions);
		AgentNoSpecialActions.put(Constants.ROGUE_ID, rogueActions);	
	}
	
	// ===================================================================================================
	// MAKEATTRIBUTES()
	// --------------------------------------------------------------------------------------------------
	// Creates static attributes of each agent.
	// ===================================================================================================
	private void makeAttributes()
	{
		AgentAttributes = new HashMap<>();
		
		// Agent info
		Integer[] enemyAttributes =
		{
			new Integer(Constants.ENEMY_HEALTH), new Integer(Constants.ENEMY_DAMAGE), new Integer(Constants.ENEMY_SPECIAL_COUNT)
		};
		Integer[] healerAttributes =
		{
			new Integer(Constants.HEALER_HEALTH), new Integer(Constants.HEALER_DAMAGE), new Integer(Constants.HEALER_SPECIAL_COUNT)
		};
		Integer[] warriorAttributes =
		{
			new Integer(Constants.WARRIOR_HEALTH), new Integer(Constants.WARRIOR_DAMAGE), new Integer(Constants.WARRIOR_SPECIAL_COUNT)
		};
		Integer[] rogueAttributes =
		{
			new Integer(Constants.ROGUE_HEALTH), new Integer(Constants.ROGUE_DAMAGE), new Integer(Constants.ROGUE_SPECIAL_COUNT)
		};
		
		// Add to hash map of attributes
		AgentAttributes.put(Constants.ENEMY_ID, enemyAttributes);
		AgentAttributes.put(Constants.HEALER_ID, healerAttributes);
		AgentAttributes.put(Constants.WARRIOR_ID, warriorAttributes);
		AgentAttributes.put(Constants.ROGUE_ID, rogueAttributes);	
	}
	
	// ===================================================================================================
	// MAKEREWARDS()
	// --------------------------------------------------------------------------------------------------
	// Create death rewards for each agent.
	// ===================================================================================================
	private void makeRewards()
	{
		AgentDeathRewards = new double[Constants.NUM_AGENTS][Constants.NUM_AGENTS];
		
		// ENEMY AGENT
		AgentDeathRewards[Constants.ENEMY_ID][Constants.ENEMY_ID] = -500.0;
		AgentDeathRewards[Constants.ENEMY_ID][Constants.WARRIOR_ID] = 60.0;
		AgentDeathRewards[Constants.ENEMY_ID][Constants.HEALER_ID] = 80.0;
		AgentDeathRewards[Constants.ENEMY_ID][Constants.ROGUE_ID] = 60.0;
		
		// HEALER AGENT
		AgentDeathRewards[Constants.HEALER_ID][Constants.HEALER_ID] = -200.0;
		AgentDeathRewards[Constants.HEALER_ID][Constants.WARRIOR_ID] = -100.0;
		AgentDeathRewards[Constants.HEALER_ID][Constants.ROGUE_ID] = -100.0;
		AgentDeathRewards[Constants.HEALER_ID][Constants.ENEMY_ID] = 400.0;
		
		// WARRIOR AGENT
		AgentDeathRewards[Constants.WARRIOR_ID][Constants.WARRIOR_ID] = -200.0;
		AgentDeathRewards[Constants.WARRIOR_ID][Constants.HEALER_ID] = -100.0;
		AgentDeathRewards[Constants.WARRIOR_ID][Constants.ROGUE_ID] = -100.0;
		AgentDeathRewards[Constants.WARRIOR_ID][Constants.ENEMY_ID] = 400.0;
		
		// ROGUE AGENT
		AgentDeathRewards[Constants.ROGUE_ID][Constants.ROGUE_ID] = -200.0;
		AgentDeathRewards[Constants.ROGUE_ID][Constants.WARRIOR_ID] = -100.0;
		AgentDeathRewards[Constants.ROGUE_ID][Constants.HEALER_ID] = -100.0;
		AgentDeathRewards[Constants.ROGUE_ID][Constants.ENEMY_ID] = 400.0;

	}
	
	// ===================================================================================================
	// MAKEAGGROREWARDS()
	// --------------------------------------------------------------------------------------------------
	// Add rewards for what aggro the agent is in (makes them favor certain actions over others)
	// ===================================================================================================
	private void makeAggroRewards()
	{
		AgentAggroRewards = new double[AGGRO_MAX_BOUND];	
		AgentAggroRewards[Aggros.LOW_AGGRO.ordinal()] = -1.0;
		AgentAggroRewards[Aggros.MEDIUM_AGGRO.ordinal()] = 50.0;
		AgentAggroRewards[Aggros.HIGH_AGGRO.ordinal()] = 100.0;	
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
}
