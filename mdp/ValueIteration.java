package mdp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import utility.Action;
import utility.MDPMap;
import utility.State;
import utility.Tuple;

// =========================================================================================================
// ValueIteration.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// Uses the MDP class to perform value iteration over the state space.
// =========================================================================================================

public class ValueIteration 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	// ===================================================================================================
	// ATTRIBUTES
	// ===================================================================================================
	private MDP myMDP;
	private int iterations;
	private double discount;
	private int agentID;
	private MDPMap<State, Double> values; // Has default values of 0.0
	private MDPMap<Action, Double> QValues; // Has default values of 0.0
	private HashMap<State, Action> policies; // Action doesn't have compareTo, so can't use MDPMap
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	public ValueIteration(MDP myMDP, int iterations, double discount, int agentID)
	{
		this.myMDP = myMDP;
		this.iterations = iterations;
		this.discount = discount;
		this.agentID = agentID;
		
		values = new MDPMap<State, Double>(0.0);
		policies = new HashMap<State, Action>();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// ITERATEVALUES()
	// --------------------------------------------------------------------------------------------------
	// Iterates over the values in order to develop a policy.
	// V(s) = max_{a in actions} Q(s,a)
	// ===================================================================================================	
	public void iterateValues()
	{
		State[][][] states = myMDP.getStates();
		int[][] stateBounds = myMDP.getStateBounds();
		double gamma = discount;
		
		// Iterations
		for(int k = 0; k < this.iterations; k++)
		{
			// Only need to iterate over the current agent
			int a = agentID;
			
			// Special Count
			for(int s = 0; s < stateBounds[myMDP.SPECIAL_BOUND][a]; s++)
			{
					// Aggro
					for(int r = 0; r < stateBounds[myMDP.AGGRO_BOUND][a]; r++)
					{
						
						State state = states[a][s][r];
						// Generate temporary QValues
						QValues = new MDPMap<Action, Double>(0.0);

						for(Action action : myMDP.getPossibleActions(state, agentID))
						{
							ArrayList<Tuple<State, Double>> stateProbs = myMDP.getTransitionAndProbs(state, action);
							
							for (Tuple<State, Double> tuple : stateProbs)
							{
								State newState = tuple.x;
								double prob = tuple.y;
								double reward = myMDP.getReward(state, action, newState, agentID);
								double QVal = QValues.get(action) + prob * ( reward + (gamma * values.get(newState)));
								QValues.put(action, QVal);
							}
							
						} // actions
						
						if(QValues.size() > 0)
						{
							Entry<Action, Double> max = QValues.getMaxValue();
							values.put(state, max.getValue());
							policies.put(state, max.getKey());
						}
						
					} // Aggro
			} // Special
		} // Iterations
	}
	
	// ===================================================================================================
	// GETPOLICY()
	// --------------------------------------------------------------------------------------------------
	// Returns the best action in the given state according to our values.
	// policy(s) = arg_max_{a in actions} Q(s,a)
	// @params State state
	// ===================================================================================================	
	public Action getPolicy(State state)
	{
		return policies.get(state);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETTERS
	// ===================================================================================================
	
	public HashMap<State, Action> getPolicies() { return policies; }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
}
