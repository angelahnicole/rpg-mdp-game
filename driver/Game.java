package driver;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Scanner;

import mdp.MDP;
import agents.*;
import utility.Action; 
import utility.Actions;
import utility.Constants;
import utility.GenMath;
import utility.State;

// =========================================================================================================
// Game.java
// --------------------------------------------------------------------------------------------------------
// Angela Gross
// CSCI-555
// Graduate Project
// --------------------------------------------------------------------------------------------------------
// This is the driver of the game. It goes through each agent, performs the agent's chosen action, and 
// informs the user of the game's status.
// =========================================================================================================

public class Game 
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// ATTRIBUTES
	// ===================================================================================================
	
	private boolean isGameOver;
	private ArrayList<Agent> agents;
	private ArrayList<Agent> killedAgents;
	private int turn;
	
	private final int NUM_AGENTS = 4;
	private final int NUM_PARTY_AGENTS = NUM_AGENTS - 1;
	
	private MDP mdp;
	
	Scanner myScanner;
	
	// Singleton
	private static Game instance;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CONSTRUCTOR
	// ===================================================================================================
	private Game()
	{
		// Init
		agents = new ArrayList<Agent>();
		killedAgents = new ArrayList<Agent>();
		isGameOver = false; 
		turn = -1;
		mdp = new MDP();
		myScanner = new Scanner(System.in);
		
		// Create enemy, warrior, rogue, and healer
		createAgents();
	}
	
	// ===================================================================================================
	// GETINSTANCE()
	// ===================================================================================================
	public static Game getInstance()
	{
		if(instance == null)
		{
			instance = new Game();
		}
		
		return instance;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// RUNGAME()
	// --------------------------------------------------------------------------------------------------
	// It first welcomes the user to the game, and then begins the main game loop. It first retrieves the
	// next agent and, if the agent is alive, then continue on with the agent's turn. During an agent's 
	// turn an agent chooses an action, prints it, performs it, and then it prints all of the agent's
	// health again. It then checks if the game has ended based upon the current game state.
	// ===================================================================================================
	public void runGame()
	{	
		// Print welcome and initial health
		printStartGame();
		printHealth();
		printTypeToContinue();
		
		// Create policy for each agent
		for(Agent agent: agents)
			agent.createPolicy();
		
		printPolicies();
		
		do
		{
			// Get get next agent, prepare for their turn, and print whose turn it is
			Agent currAgent = getNextAgent();
			
			if(!isKilled(currAgent))
			{
				currAgent.setupTurn();
				printTurn();
				
				// Get next action and perform it and print it
				Action agentAction = currAgent.chooseMove();
				printAction(agentAction);
				performAction(agentAction);
				
				printHealth();
				
				// Check if game has ended
				if( hasGameEnded(agentAction) )
					endGame();
			}
			else
			{
				printPlayerDead(currAgent);
			}
		}
		while(!isGameOver);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// CREATEAGENTS()
	// --------------------------------------------------------------------------------------------------
	// Creates enemy, rogue, warrior, and healer agents based on the attributes in utility.Constants and
	// stores them in the game's local ArrayList.
	// ===================================================================================================
	private void createAgents()
	{
		// Make agents
		Agent enemy = new EnemyAgent();
		Agent rogue = new RogueAgent();
		Agent warrior = new WarriorAgent();
		Agent healer = new HealerAgent();
		
		// Store them
		agents.add(enemy);
		agents.add(healer);
		agents.add(rogue);
		agents.add(warrior);	
	}
	
	// ===================================================================================================
	// GETNEXTAGENT()
	// --------------------------------------------------------------------------------------------------
	// Using a simple "turn" integer, it cycles through the list of agents.
	// @returns Agent
	// ===================================================================================================
	private Agent getNextAgent()
	{
		int turnTest = turn + 1;
		
		// Cycle back if we are on the last agent (or if it's the first turn)
		if(turnTest >= agents.size() || turn == -1)
		{
			turn = 0;
		}
		else
		{
			turn++;
		}
		
		return agents.get(turn);
	}
	
	// ===================================================================================================
	// PERFORMACTION()
	// --------------------------------------------------------------------------------------------------
	// Switches on the actionType and performs the necessary action depending on it. If the targeted
	// agent is dead, then the action automatically fails.
	// @params Action action
	// ===================================================================================================
	private void performAction(Action action)
	{
		Agent sourceAgent = agents.get( action.getSourceAgent() );
		Agent targetAgent = agents.get( action.getTargetAgent() );
		Actions actionType = action.getActionType();
		boolean succeeded = GenMath.getOutcome(action.getSourceChanceToHit());
		
		// Fail actions that target dead agents
		if(killedAgents.contains(targetAgent))
			succeeded = false;
		
		switch(actionType)
		{
			case ATTACK_ACTION:
			{
				if(succeeded)
				{
					// If successful, then increase aggro of source and subtract health from target
					sourceAgent.updateAggro(action);
					targetAgent.subtractHealth( sourceAgent.getDamage() );	
				}
				
				break;
			}
			case DEFEND_ACTION:
			{
				if(succeeded)
				{
					// If successful, then increase aggro of source and flip isDefending boolean of target
					sourceAgent.updateAggro(action);
					targetAgent.makeDefendingTrue();	
				}
				
				break;
			}
			case SPECIAL_HEAL_ACTION:
			{
				if(succeeded)
				{
					// If successful, then increase aggro of source and add health to target
					sourceAgent.updateAggro(action);
					targetAgent.addHealth(Constants.HEAL_AMOUNT);
					sourceAgent.decrementSpecialCount();
				}
				
				break;
			}
			case SPECIAL_TAUNT_ACTION:
			{
				if(succeeded)
				{
					// If successful, then increase aggro of target (only works on warrior)
					targetAgent.updateAggro(action);
					sourceAgent.decrementSpecialCount();
				}
				
				break;
			}
			case SPECIAL_TRICK_ACTION:
			{
				if(succeeded)
				{
					// If successful, then decrease aggro of target (only works on rogue)
					targetAgent.updateAggro(action);
					sourceAgent.decrementSpecialCount();
				}
				
				break;
			}	
		}
		
		printOutcome(succeeded);
		
	}
	
	// ===================================================================================================
	// ENDGAME()
	// --------------------------------------------------------------------------------------------------
	// Sets the isGameOver boolean to true, prints the health statuses of each agent, prints who won,
	// and prints the end game text.
	// ===================================================================================================
	private void endGame()
	{
		isGameOver = true;
		printHealth();
		printWinOrLose(killedAgents.size() != NUM_PARTY_AGENTS);
		printEndGame();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// HASGAMEENDED()
	// --------------------------------------------------------------------------------------------------
	// Checks if the game has ended by seeing if the enemy or all of the party members are dead. If it
	// sees that the target party member of the most recent action has died but isn't in the killedAgents 
	// list, then it adds that member to the list.
	// @params Action action
	// @returns boolean gameEnded
	// ===================================================================================================
	private boolean hasGameEnded(Action action)
	{
		Agent targetAgent = agents.get( action.getTargetAgent() );
		boolean gameEnded = false;
		
		if(! (targetAgent instanceof EnemyAgent) )
		{
			if(targetAgent.isDead() && !isKilled(targetAgent))
			{
				killedAgents.add(targetAgent);
			}
			
			if(killedAgents.size() == NUM_PARTY_AGENTS)
				gameEnded = true;
		}
		else
		{
			if( targetAgent.isDead() )
				gameEnded = true;
		}
		
		return gameEnded;
	}
	
	// ===================================================================================================
	// ISKILLED()
	// --------------------------------------------------------------------------------------------------
	// Does a quick check in the killedAgents list to see if the agent is killed. (This is more like a 
	// check to see if it's in the list since we do have a "isDead" attribute for each agent)
	// @params Agent agent
	// @returns boolean isKilled
	// ===================================================================================================
	private boolean isKilled(Agent agent)
	{
		return killedAgents.contains(agent);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// PRINTERS
	// --------------------------------------------------------------------------------------------------
	// Methods that print various aspects of the game to the console
	// ===================================================================================================
	
	private void printPlayerDead(Agent agent)
	{
		System.out.println("------------------------------------------------------------------------------");
		System.out.println(agent + " is dead. Next turn.");
		System.out.println();
	}
	
	private void printStartGame()
	{
		System.out.println("==============================================================================");
		System.out.println("Welcome! Game Start!");
		System.out.println("==============================================================================");
		System.out.println();
	}
	
	private void printEndGame()
	{
		System.out.println();
		System.out.println("==============================================================================");
		System.out.println("Farewell! Game End!");
		System.out.println("==============================================================================");
	}
	
	private void printTurn()
	{
		Agent currAgent = agents.get(turn);
		System.out.println();
		System.out.println("------------------------------------------------------------------------------");
		System.out.println("*** IT IS NOW " + currAgent +"'S TURN ***");
	}
	
	private void printAction(Action action)
	{
		Agent sourceAgent = agents.get( action.getSourceAgent() );
		Agent targetAgent = agents.get( action.getTargetAgent() );
		Actions actionType = action.getActionType();
		String actionText = "" + sourceAgent;
		
		switch(actionType)
		{
			case ATTACK_ACTION:
			{
				actionText += " attacks " + targetAgent + " for " + sourceAgent.getDamage();
				break;
			}
			case DEFEND_ACTION:
			{
				actionText += " defends " + targetAgent + " for " + Constants.DEFEND_AMOUNT;
				break;
			}
			case SPECIAL_HEAL_ACTION:
			{
				actionText += " heals " + targetAgent + " for " + Constants.HEAL_AMOUNT;
				break;
			}
			case SPECIAL_TAUNT_ACTION:
			{
				actionText += " taunts " + targetAgent;
				break;
			}
			case SPECIAL_TRICK_ACTION:
			{
				actionText += " tricks " + targetAgent;
				break;
			}	
		}
		
		System.out.println(actionText);
	}
	
	private void printHealth()
	{
		System.out.println("+++");
		
		for(Agent agent : agents)
		{
			if(agent.isDead())
				System.out.println(agent + "'s HEALTH: DEAD");
			else
				System.out.println(agent + "'s HEALTH: "  + agent.getHealth() );
		}
		System.out.println("+++\n");
	}
	
	private void printOutcome(boolean succeeded)
	{
		String outcomeText;
		
		if(succeeded)
			outcomeText = "It succeeds!";
		else
			outcomeText = "It fails!";
		
		System.out.println(outcomeText);
	}
	
	private void printWinOrLose(boolean win)
	{
		String winOrLoseText = "";
		
		if(win)
			winOrLoseText = " The party wins!! The enemy has been defeated!";
		else
			winOrLoseText = " The party fails! The enemy has prevailed.";
		
		System.out.println("[][][][][][][][][][][][][][][][][][][][][][][]");
		System.out.println(winOrLoseText);
		System.out.println("[][][][][][][][][][][][][][][][][][][][][][][]");
		System.out.println();
	}
	
	private void printPolicies()
	{
		System.out.println();
		System.out.println("================================== POLICIES ==================================");
		
		for(Agent agent : agents)
		{
			System.out.println();
			System.out.println("------------------------------------------------------------------------------");
			System.out.println(agent + "'s POLICY -> s = {agent, aggro, special}, a = {target, action}");
			
			for(Entry<State, Action> policy: agent.getPolicies().entrySet())
			{			
				// Get state and action
				State state = policy.getKey();
				Action action = policy.getValue();
				// Print them
				System.out.println("s = {" + agents.get(state.getAgent()) + ", " + state.getAggro() + ", " + state.getSpecial() + "}, "
									+ "a = {" + agents.get(action.getTargetAgent()) + ", " + action.getActionType()+ "}");
			}
			
			// Allows to see all policies without scrolling
			printTypeToContinue();
		}
	}
	
	private void printTypeToContinue()
	{
		
		System.out.println();
		System.out.println("Please press [Enter] to continue.");
		myScanner.nextLine();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ===================================================================================================
	// GETTERS
	// ===================================================================================================
	
	public boolean isGameOver() { return isGameOver; }
	public ArrayList<Agent> getAgents() { return agents; }
	public MDP getMDP() { return mdp; }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
}
